#include "../libs/csort.h"

void bubblesort(int *array, int length)
{
	int active = 1;
	for (int i = 0; active; i++)
	{
		active = 0;
		for (int j = 0; j < length - i - 1; j++)
		{
			if (array[j] > array[j + 1])
			{
				active = 1;
				swap(array + j, array + j + 1);
			}
		}
	}
}
