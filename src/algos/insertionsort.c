#include "../libs/csort.h"

void insertionsort(int *array, int n)
{
    int j;
    for (int i = 1; i < n; i++)
    {
        j = i;
        while ((j > 0) && (array[j] < array[j - 1]))
        {
            swap(array + j, array + j - 1);
            j--;
        }
    }
}
