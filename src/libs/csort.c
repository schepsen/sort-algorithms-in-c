#ifndef _POSIX_C_SOURCE
# define _POSIX_C_SOURCE 201101L
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "csort.h"

#define DEBUG 0
#define BENCHMARK 5

void init(void)
{
    srand(time(NULL));
}

void swap(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

void show(int *array, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", array[i]);
    }
    printf("%s", "\n");
}

void benchmark(void (*sort)(int *, int), int *array, int n)
{
    struct timespec t1, t2;
    long measured[BENCHMARK];
    int *temp = (int *) malloc(n * sizeof(int));

    for (int i = 0; i < BENCHMARK; i++)
    {
        memcpy(temp, array, n * sizeof(int));
        if (DEBUG)
        {
            show(temp, n);
        }
        clock_gettime(CLOCK_MONOTONIC, &t1);
        sort(temp, n);
        clock_gettime(CLOCK_MONOTONIC, &t2);
        if (DEBUG)
        {
            show(temp, n);
        }
        measured[i] = (t2.tv_sec - t1.tv_sec) * (1000000000l) + (t2.tv_nsec - t1.tv_nsec);
        printf("Pass #%02d: %ld ns (%.02f s)\n", i, measured[i], measured[i] / 1000000000.0);
    }
    printf("-------------------------------------------------------\n");
    free(temp);
}
