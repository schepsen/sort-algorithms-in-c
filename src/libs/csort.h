#ifndef CSORTLIB_H_
#define CSORTLIB_H_

void show(int *array, int length);
void init(void);
void benchmark(void (*f)(int *, int), int *array, int n);

void swap(int *x, int *y);

void bubblesort(int *array, int size);
void selectionsort(int *array, int size);
void insertionsort(int *array, int size);

#endif /* CSORTLIB_H_ */
