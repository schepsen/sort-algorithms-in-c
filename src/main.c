#include <stdlib.h>
#include <stdio.h>

#include "libs/csort.h"

int main(int argc, char **argv)
{
    printf("%s\n", "-------------------------------------------------------");
    printf("%s\n", "# Copyright (C) 2014 Nikolaj Schepsen");
    printf("%s\n", "# Project: Kapitel 2: Sortieren in C");
    printf("%s\n", "# Release: 1.0");
    printf("\n");
    printf("%s\n", "# Repository: git@bitbucket.org:schepsen/c-kapitel-2-sortieren-in-c.git");
    printf("%s\n", "# Contact: schepsen@web.de");
    printf("\n");
    printf("%s\n", "# CMAKE 2.8 http://www.cmake.org/cmake/resources/software.html");

    printf("%s\n", "-------------------------------------------------------");
    if (argc != 2)
    {
        printf("usage: %s size (The size of array)\n", argv[0]);
        return EXIT_FAILURE;
    }
    int size = atoi(argv[1]), array[size];

    init();
    for (int i = 0; i < size; i++)
    {
        array[i] = rand();
    }

    printf("# Trying to sort an array with %d elements using Bubble Sort\n", size);
    printf("%s\n", "-------------------------------------------------------");
    benchmark(&bubblesort, array, size);

    printf("# Trying to sort an array with %d elements using Selection Sort\n", size);
    printf("%s\n", "-------------------------------------------------------");
    benchmark(&selectionsort, array, size);

    printf("# Trying to sort an array with %d elements using Insertion Sort\n", size);
    printf("%s\n", "-------------------------------------------------------");
    benchmark(&insertionsort, array, size);

    return EXIT_SUCCESS;
}
